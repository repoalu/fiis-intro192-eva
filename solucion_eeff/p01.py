import random
'''
- Funcion que genera un numero aleatorio en en rango dado.
- Pocos validaron la entrada de datos, es importante tenerlo en cuenta.
- No he considerado obligatorio el llamado a randint, pueden por ejemplo tener una lista de valores posibles
y retornar cualquiera de ellos (pueden usar un contador global para que el valor sea diferente)
'''
def generar_aleatorio(minimo, maximo):
    if(minimo < 1):
        return None
    elif(minimo > 100):
        return None
    else:
        return random.randint(minimo, maximo)

def adivinar_numero():
    continuar = True
    numero_adivinar = generar_aleatorio(1, 100)
    print("[Para pruebas] Num. Aleatorio:", numero_adivinar)
    intentos = 0
    while(continuar == True):
        valor = int(input ("Ingrese el valor del numero secreto: "))
        if(valor > numero_adivinar):
            print("Valor ingresado es mayor que el numero secreto")
        elif(valor < numero_adivinar):
            print("Valor ingresado es menor que el numero secreto")
        else:
            print("Respuesta correcta")
            continuar = False
        intentos = intentos + 1
    print("Cantidad de intentos: ", intentos)

#Pruebas
def main():
    adivinar_numero()

main()