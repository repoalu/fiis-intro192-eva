
def obtener_max_mineral(mat_recursos):
    #Debo almacenar el maximo valor, asi como la fila y columna que resulta    
    maximo = 0
    max_x = 0
    max_y = 0
    #Los 2 bucles siguientes me permiten recorrer la matriz elemento por elemento
    for i in range(len(mat_recursos)):
        for j in range(len(mat_recursos[i])):
            '''
            La variable "mayor" representa la cantidad maxima de mineral que podemos recolectar.
            mayor = valor_matriz + max(valor_adyacente)
            '''
            mayor = 0
            #izquierda. Nos cuidamos de no salir de los limites en los valores de indices (indice < 0)
            if(i > 0):
                izquierda = mat_recursos[i - 1][j]
                if(izquierda > mayor):
                    mayor = izquierda
            #derecha. Nos cuidamos de no salir de los limites en los valores de indices (indice == longitud)
            if(i < len(mat_recursos[i]) - 1):
                derecha = mat_recursos[i + 1][j]
                if(derecha > mayor):
                    mayor = derecha
            #arriba. Nos cuidamos de no salir de los limites en los valores de indices (indice == longitud)
            if(j < len(mat_recursos) - 1):
                arriba =  mat_recursos[i][j + 1]
                if(arriba > mayor):
                    mayor = arriba
            #abajo. Nos cuidamos de no salir de los limites en los valores de indices (indice < 0)
            if(j > 0):
                abajo = mat_recursos[i][j - 1]
                if(abajo > mayor):
                    mayor = abajo
            
            #Suma reprsenta la cantidad maxima de mineral a recolectar si aterrizamos en la celda i, j
            suma = mat_recursos[i][j] + mayor
            
            #Comparamos con el valor maximo que tenemos hasta el momento
            if(suma > maximo):
                maximo = suma
                max_x = i
                max_y = j
                
    print("Cantidad maxima de mineral:", maximo)
    print("Coordenadas:",max_x,max_y)

#Pruebas
def main():
    recursos =  [[30, 80, 50, 10],
                 [50, 20, 90, 30],
                 [20, 30, 10, 90],
                 [20, 80, 80, 20]]
    obtener_max_mineral(recursos)
    
main()