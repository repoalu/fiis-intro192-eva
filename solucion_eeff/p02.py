# -*- coding: utf-8 -*-

'''
Dado un texto, obtiene la lista de las palabras que lo conforman
Es equivalente al uso de texto.split()
'''
def obtener_palabras(texto):
    respuesta = []
    palabra = ""
    for i in range(len(texto)):
        if(texto[i] == " "):
            respuesta.append(palabra)
            palabra = ""
        else:
            palabra = palabra + texto[i]
    respuesta.append(palabra)
    return respuesta

def obtener_palabras2(texto):
    return texto.split()


'''
Permite convertir el texto para evaluar coincidencias "similares".
Para este caso, todo el texto en convertido a mayusculas y se reemplazan las ocurrencias con tilde por vocales sin tilde.
Como alternativa, puede trabajarse con cadena.upper() para la conversión a mayúsculas
'''
def convertir_palabra(texto):
    # Manejamos equivalencias de letras en una lista, para poder convertir
    letras = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
              "á","é","í","ó","ú","Á","É","Í","Ó","Ú"]
    reemplazo = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
                 "A","E","I","O","U","A","E","I","O","U"]
    
    respuesta = ""
    
    #Por cada letra del texto buscamos en la lista de letras. Alternativamente podemos usar "index".
    for i in range(len(texto)):
        encontrado = False
        for j in range(len(letras)):
            if(texto[i] == letras[j]):
                encontrado = True
                respuesta = respuesta + reemplazo[j]
        if(encontrado == False):
            respuesta = respuesta + texto[i]
    return respuesta
    
'''
Compara los textos para encontrar plagio
Por cada elemento de la lista1 (palabras del texto1) buscará en la lista2 (palabras del texto2). Si encuentra una coincidencia, a partir de ahí
empezará a buscar palabras de forma consecutiva para contar si existen 5 coincidencias por lo menos.
'''

def comparar_textos(texto1, texto2):
    lista1 = obtener_palabras(texto1)
    lista2 = obtener_palabras(texto2)
    # Por cada palabra de lista1
    for i in range(len(lista1) - 4):
        #Busco en la lista2
        for j in range(len(lista2) - 4):
            #Convertimos para manejar tildes y mayúsculas
            palabra1 = convertir_palabra(lista1[i])
            palabra2 = convertir_palabra(lista2[i])
            if(palabra1 == palabra2):
                #Vemos que hay una coincidencia, seguimos buscando a ver si encontramos las 5 coincidencias
                contador = 1
                for k in range(1, 5):
                    palabra1 = convertir_palabra(lista1[i + k])
                    palabra2 = convertir_palabra(lista2[j + k])
                    if(palabra1 == palabra2):
                        contador = contador + 1
                    else:
                        break
                #Si llego a contar 5 palabras consecutivas vemos que hay plagio
                if(contador == 5):
                    return 1
    return 0
#Pruebas
def main():
    texto1 = "Pasadas las 5 los alumnes estuvieron jugando en el salón"
    texto2 = "Pasadas las 9 los alumnos estuvieron jugando en el Salon"
    res = comparar_textos(texto1, texto2)
    if(res == 0):
        print("No hay plagio")
    else:
        print("Hay plagio")
    
main()