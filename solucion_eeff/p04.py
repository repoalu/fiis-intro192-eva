'''
Almacenamos una lista con los valores que deben generarse en funcion del residuo
'''
def generar_placa(nro):
    letras_reemp = ["Z", "A", "B", "C", "D", "E", "F", "G", "H", "I"]
    resto = nro % 10
    letra = letras_reemp[resto] 
    return str(nro) + letra
                     

def main():
    placa = input("Ingrese placa del vehiculo:")
    numero = int(placa[-3:]) #Opcion2: placa[len(placa) - 3:]
    print("Nueva placa:",generar_placa(numero))
    
main()