'''
Implemente un programa que permita leer valores positivos 
desde el teclado hasta que el usuario ingrese el valor -1.
Muestre la lista final y el mayor elemento
''' 

def leer_datos():
    respuesta = []
    continuar = True
    while(continuar == True):
        siguiente = int(input("Ingrese valor: "))
        if(siguiente == -1):
            continuar = False
        else:
            respuesta.append(siguiente)
    return respuesta

def obtener_maximo(lista):
    mayor = 0
    for elemento in lista:
        if(elemento > mayor):
            mayor = elemento
    return mayor 

def main():
    lista = leer_datos()
    maximo = obtener_maximo(lista)
    print(maximo)
#Pruebas
main()
    