def es_primo(n):
    contador = 0
    for i in range(1, n + 1):
        if(n % i == 0):
            contador = contador + 1
    if(contador == 2):
        return True
    else:
        return False

def obtener_primo(lista, k):
    contador = 0
    for i in range(len(lista)):
        if(lista[i] >= 100 and lista[i] < 1000 and es_primo(lista[i])):
            contador = contador + 1
        if(contador == k):
            return lista[i]
    if(contador < k):
        return -1

def main():
    lista = [80, 65, 337, 17, 68, 197, 120, 263, 145]
    k = 3
    primo = obtener_primo(lista, 3)
    print(k,". primo de 3 cifras:", primo )

#Pruebas
main()    
