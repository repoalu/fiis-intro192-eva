def obtener_promedio(lista):
    longitud = len(lista)
    suma = 0
    for i in range(longitud):
        suma = suma + lista[i]
    return 1.0 * suma / longitud

def comparar_valores(lista, referencia):
    mayores = 0
    menores = 0    
    for i in range(len(lista)):
        if(lista[i] > referencia):
            mayores = mayores + 1
        elif(lista[i] < referencia):
            menores = menores + 1
    print("Cant. mayores:", mayores)
    print("Cant. menores:", menores)    


def comparar_valores2(lista, referencia):
    mayores = []
    menores = []   
    for i in range(len(lista)):
        if(lista[i] > referencia):
            mayores.append(lista[i])
        elif(lista[i] < referencia):
            menores.append(lista[i])
    return [mayores, menores]


def main():
    lista = [10, 12, 11, 16, 20]
    prom = obtener_promedio(lista)
    valores = comparar_valores2(lista, prom)
    print("Mayores:", valores[0])
    print("Menores:", valores[1])

#Pruebas
main()