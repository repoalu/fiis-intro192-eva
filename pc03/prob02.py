def evaluar_orden(lista):
    alumnoi = -1
    alumnoj = -1
    for i in range(len(lista) - 1):
        if(lista[i] > lista[i + 1]):
            if(alumnoi == -1):
                alumnoi = i
            else:
                alumnoj = i + 1
    print("Valores de i, j:", alumnoi, alumnoj)
    aux = lista[alumnoi]
    lista[alumnoi] = lista[alumnoj]
    lista[alumnoj] = aux
    print(lista)

def main():
    #lista = [1.63, 1.76, 1.72, 1.68, 1.78]
    lista = [4.8,6.9,1.2,0.8,9.6,2.1]    
    print(lista)
    lista.sort()
    print(lista)
    evaluar_orden(lista)

#Prueba
main()
