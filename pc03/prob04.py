def obtener_conteo(lista):
    contador = 1
    mayor1 = 0
    mval1 = 0
    
    mayor2 = 0
    mval2 = 0

    for i in range(len(lista) - 1):
        valor1 = lista[i]
        valor2 = lista[i + 1]
        if(valor1 == valor2):
            contador = contador + 1
        else:
            if(contador > mayor1):
                mayor2 = mayor1
                mval2 = mval1 
                mayor1 = contador
                mval1 = valor1
            elif(contador > mayor2):
                mayor2 = contador
                mval2 = valor1              
            contador = 1
    print("Valor con la segunda mayor cantidad de apariciones:", mval2)
    print("Cantidad de apariciones:", mayor2)

lista = [2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 4, 7, 7, 7, 7, 7, 31]
obtener_conteo(lista)
