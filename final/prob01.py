from audioop import reverse
lista_cod = [130, 125, 280]
lista_pp = [12, 14, 11.5]


def mostrar(lista): 
    for i in range(len(lista)):
        print(lista[i])

def mostrar1():
    lista = [10, 11, 23, 44] 
    for i in range(len(lista)):
        print(lista[i])
    return lista

def agregar_alumno():
    codigo = int(input("Ingrese cod alu.:"))
    promedio = float(input("Ingrese promedio:"))
    lista_cod.append(codigo)
    lista_pp.append(promedio)

def convertir(matriz):
    resultado = []
    for i in range(len(matriz)):
        resultado = resultado + matriz[i]
    return resultado

#Pruebas
#agregar_alumno()
def prueba_listas():
    print(lista_cod)
    print(lista_pp)
    nota7 = 16
    print(lista_cod + lista_pp + [3, 4, 5] + [nota7])



def prueba_matriz():
    mat = [ [15, 85, 35],
            [45, 98, 46],
            [57, 81, 39]        
          ]
    res = convertir(mat)
    print(mat)
    print(res)
    return res

def prueba_funciones():
    res = prueba_matriz()
    print(res)
    res.reverse()
    print(res)
    res.sort(reverse = True)
    print(res)
    print("1er puesto:", res[0])
    print("2do puesto:", res[1])

def prueba_slicing():
    lista = [15, 85, 35, 45, 98, 46, 57, 81, 39]
    print(lista[2:3]) #2
    print(lista[2:4]) #2 3
    print(lista[2:]) #2 3 4 ..... ult
    print(lista[:4]) #0 1 2 3
    
    

#Pruebas
prueba_slicing()